# Module 04 - WP 3
### Nick Diamantopoulos - s2929880
[**GitLab repository**](https://gitlab.utwente.nl/s2929880/m04weblab03-bikedealer)
## Overview
This app development took the most amount of time but was also the most interesting. I found that doing the frontend was the most time consuming thing as I wanted to make it look pretty and I believe it came out looking the best. That said, it was the most frustrating and most time consuming part of the development.

## Difficulties
Most issues I had were regarding the ambiguity of the assignment. It was a bit too open to interpretation and it took me a bit of time to get accustomed to it.

Coding was relatively easy as I already have quite a bit of experience with frontend and java. I did have a few difficulties working with the backend as I haven't gotten too accustomed to jersey yet.


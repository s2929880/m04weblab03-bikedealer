package nl.utwente.di.dao;

import nl.utwente.di.model.Bike;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum BikeDao {
    instance;
    private final Map<String, Bike> bikes = new HashMap<>();
    BikeDao() {
        bikes.put("01", new Bike("01", "John", "black", "helicopter"));
    }
    public List<Bike> getAll() {return new ArrayList<>(bikes.values());};

    public List<Bike> getByGender(String gender) {
        if(gender.equals("")) {
            return getAll();
        }
        return bikes.values().stream().filter(bike -> bike.getGender().equals(gender)).collect(Collectors.toList());
    }

    public List<Bike> getByColour(String colour) {
        if(colour.equals("")) {
            return getAll();
        }
        return bikes.values().stream().filter(bike -> bike.getColour().equals(colour)).collect(Collectors.toList());
    }
    public Bike getByID(String id) {
        return bikes.get(id);
    }
    public synchronized void addBike(Bike bike){
        bikes.put(bike.getBikeID(),bike);
    }
    public synchronized void removeBike(String bikeid){
        bikes.remove(bikeid);
    }

    public void updateBike(String id, Bike bike) {
        bikes.put(id,bike);
    }

    public boolean contains(String id) {
        return bikes.containsKey(id);
    }
}

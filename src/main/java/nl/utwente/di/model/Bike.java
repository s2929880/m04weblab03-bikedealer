package nl.utwente.di.model;

import jakarta.xml.bind.annotation.XmlRootElement;

import java.math.BigDecimal;

@XmlRootElement
public class Bike {
    private String bikeID;
    private String ownerName;
    private String colour;
    private String gender;
    public Bike(){}
    public Bike(String bikeID, String ownerName, String colour, String gender) {
        setBikeID(bikeID);
        setOwnerName(ownerName);
        setColour(colour);
        setGender(gender);
    }

    public String getBikeID() {
        return bikeID;
    }

    public void setBikeID(String bikeID) {
        this.bikeID = bikeID;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}

package nl.utwente.di.resources;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.xml.bind.annotation.XmlRootElement;
import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

import java.util.List;
import java.util.stream.Collectors;

@Path("/bikes")
@XmlRootElement
public class BikeResource {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createBike(Bike bike) {
        BikeDao.instance.addBike(bike);
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bike> getBikes(@DefaultValue("") @QueryParam("colour") String colour, @DefaultValue("") @QueryParam("gender") String gender) {
        List<Bike> colourSorted = BikeDao.instance.getByColour(colour);
        List<Bike> genderSorted = BikeDao.instance.getByGender(gender);
        return colourSorted.stream().filter(genderSorted::contains).collect(Collectors.toList());
    }

    @Path("{bikeid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Bike getBikeDetails(@PathParam("bikeid") String id) {
        return BikeDao.instance.getByID(id);
    }
    @Path("{bikeid}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateBike(@PathParam("bikeid") String id, Bike bike) {
        BikeDao.instance.updateBike(id,bike);
    }
    @Path("{bikeid}")
    @DELETE
    public void deleteBike(@PathParam("bikeid") String id) {
        BikeDao.instance.removeBike(id);
    }
    @Path("{bikeid}/order")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String orderBike(@PathParam("bikeid") String id) {
        if(BikeDao.instance.contains(id)) {
            BikeDao.instance.removeBike(id);
            return "Your order has been successful!";
        }
        return "Your order was unsuccessful!";
    }
}

const api = "http://localhost:8080/bikeDealer_war/api";
const getField = (id) => document.getElementById(id).value;
const onSell = async () => {
	const id = getField("bikeInformationID");
	const name = getField("bikeInformationName");
	const colour = getField("bikeInformationColour");
	const gender = getField("bikeInformationGender");
	console.log({
		bikeID: id,
		ownerName: name,
		colour: colour,
		gender: gender,
	});
	await fetch(`${api}/bikes`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			bikeID: id,
			ownerName: name,
			colour: colour,
			gender: gender,
		}),
	});
	window.location.href = "../offers";
};

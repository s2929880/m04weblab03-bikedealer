const api = "http://localhost:8080/bikeDealer_war/api";
const getBikes = async () => {
	const colour = document.getElementById("filterColour").value;
	const gender = document.getElementById("filterGender").value;
	console.log(`${api}/bikes?colour=${colour}&gender=${gender}`);
	const bikes = await fetch(
		`${api}/bikes?colour=${colour}&gender=${gender}`,
		{
			method: "GET",
		}
	).then((response) => response.json());
	document.getElementById("dataTable").innerHTML =
		"<tr><th>ID</th><th>Owner</th><th>Color</th><th>Gender</th><th>Order</th><th>Delete</th></tr>";
	bikes.forEach((element) => {
		createBikeEntry(element);
	});
};

const createBikeEntry = (bike) => {
	const bikeTable = document.getElementById("dataTable");
	const bikeEntry = document.createElement("tr");
	bikeEntry.id = `bikeEntry_${bike.bikeID}`;
	bikeEntry.innerHTML = `<tr><td>${bike.bikeID}</td>
		<td>${bike.ownerName}</td>
		<td>${bike.colour}</td>
		<td>${bike.gender}</td>
        <td><button class="queryButtons" onclick="orderBike('${bike.bikeID}')"><span class="material-icons">
        shopping_cart
        </span></button></td>
        <td><button class="queryButtons" onclick="deleteBike('${bike.bikeID}')"><span class="material-icons">
        delete_forever
        </span></button></td></tr>`;
	bikeTable.appendChild(bikeEntry);
};
const orderBike = async (bikeID) => {
	await fetch(`${api}/bikes/${bikeID}/order`, {
		method: "POST",
	})
		.then((response) => response.text())
		.then((text) => alert(text));
};
const deleteBike = async (bikeID) => {
	await fetch(`${api}/bikes/${bikeID}`, {
		method: "DELETE",
	});
	getBikes();
};
